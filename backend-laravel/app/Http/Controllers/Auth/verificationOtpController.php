<?php

namespace App\Http\Controllers\Auth;

use App\Otp_code;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class verificationOtpController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'otp' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        };
        $otp_code = Otp_code::where('otp_code', $request->otp)->first();
        if (!$otp_code) {
            return response()->json([
                'succes' => false,
                'message' => 'otp code tidak ada'
            ], 400);
        }

        if ($otp_code->valid_until < Carbon::now()) {
            return response()->json([
                'succes' => false,
                'message' => 'otp code sudah kadaluarsa'
            ], 404);
        }
        $user = $otp_code->user();
        $user->update([
            'email_verified_at' => Carbon::now()
        ]);

        $otp_code->delete();

        return response()->json([
            'success' => true,
            'message' => 'verifikasi email berhasil'
        ], 200);
    }
}
