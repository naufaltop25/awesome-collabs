<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Otp_code;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Events\GenerateOtpCode;
use App\Mail\GenerateOtpCodeMail;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class regenerateOtpController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $user = User::where('email', $request->email)->first();
        if ($user->otp_code) {
            $user->otp_code->delete();
        }

        do {
            $otp = mt_rand(100000, 999999);
            $check = Otp_code::where('otp_code', $otp)->first();
        } while ($check);

        $validUntil = Carbon::now()->addMinutes(5);
        $otpCode = Otp_code::create([
            'otp_code' => $otp,
            'valid_until' => $validUntil,
            'user_id' => $user->id
        ]);
        event(new GenerateOtpCode($otpCode));
        // Mail::to($otpCode->user->email)->send(new GenerateOtpCodeMail($otpCode));
        return response()->json([
            'succes' => true,
            'message' => 'otp code telah dibuat ulang, silahkan cek email anda',
            'data' => [
                'user' => $user,
                'otp' => $otpCode
            ]
        ], 200);
    }
}
