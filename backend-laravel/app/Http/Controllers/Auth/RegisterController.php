<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Otp_code;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Events\RegisterOtpCode;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class registerController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        //
        $validator = Validator::make($request->all(), [
            'name'      => 'required',
            'email'     => 'required|unique:users,email',
            'username'  => 'required|unique:users,username'
        ]);

        // response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }
        $user = User::create($request->all());

        do {
            $otp = mt_rand(100000, 999999);
            $check = Otp_code::where('otp_code', $otp)->first();
        } while ($check);

        $validUntil = Carbon::now()->addMinutes(5);
        $otpCode = Otp_code::create([
            'otp_code' => $otp,
            'valid_until' => $validUntil,
            'user_id' => $user->id
        ]);
        event(new RegisterOtpCode($otpCode));
        return response()->json([
            'succes' => true,
            'message' => 'user telah dibuat, silahkan verifikasi',
            'data' => [
                'user' => $user,
                'otp' => $otpCode
            ]
        ], 200);
    }
}
