<?php

namespace App\Http\Controllers;

use App\Suka;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class SukaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $suka = Suka::latest()->get();

        // make json response
        return response()->json([
            'succes' => true,
            'message' => 'list data suka',
            'data' => $suka
        ], 200);
    }



    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'like'   => 'required',
            'berita_id' => 'required',
        ]);

        // response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }
        // save to database
        $suk = Suka::create([
            'like' => $request->like,
            'berita_id' => $request->berita_id,
            'user_id' => auth()->user()->id,
        ]);
        if ($suk) {
            return response()->json([
                'succes' => true,
                'message' => 'like berhasil di buat',
                'data' => $suk
            ], 201);
        }
        return response()->json([
            'succes' => false,
            'message' => 'like data failed to save'
        ], 409);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $like = Suka::findOrfail($id);

        // make json response
        return response()->json([
            'succes' => true,
            'message' => 'detail suka',
            'data' => $like
        ], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'like'   => 'required',
            'berita_id' => 'required',
        ]);

        // response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }
        $like = Suka::findOrFail($id);
        if ($like) {
            if (auth()->user()->id == $like->user->id) {
                # code...
                $like->update([
                    'like' => $request->like,
                    'berita_id' => $request->berita_id
                ]);
                return response()->json([
                    'succes' => true,
                    'message' => 'like berhasil di updated',
                    'data' => $like
                ], 200);
            }
            return response()->json([
                'succes' => false,
                'message' => 'forbidden'
            ], 403);
        }
        return response()->json([
            'succes' => false,
            'message' => 'like not found'
        ], 404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $like = Suka::findOrFail($id);
        // deleted comment
        if ($like) {
            if (auth()->user()->id == $like->user->id) {
                # code...
                $like->delete();

                return response()->json([
                    'succes' => true,
                    'message' => 'like berhasil di hapus'
                ], 200);
            }
            return response()->json([
                'succes' => false,
                'message' => 'forbidden'
            ], 403);
        }
        // response failed to delete
        return response()->json([
            'succes' => false,
            'message' => 'like not found'
        ], 404);
    }
}
