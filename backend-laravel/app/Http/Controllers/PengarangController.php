<?php

namespace App\Http\Controllers;

use App\Pengarang;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class komentarController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pengarang = Pengarang::latest()->get();

        // make json response
        return response()->json([
            'succes' => true,
            'message' => 'list data pengarang',
            'data' => $pengarang
        ], 200);
    }



    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'nama' => 'required',
            'email' => 'required',
            'username' => 'required',
            'password' => 'required',
        ]);

        // response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }
        // save to database
        $peng = Pengarang::create([
            'nama' => $request->nama,
            'email' => $request->email,
            'username' => $request->username,
            'password' => $request->password,
            'user_id' => auth()->user()->id,
        ]);
        if ($peng) {
            return response()->json([
                'succes' => true,
                'message' => 'pengarang berhasil di buat',
                'data' => $peng
            ], 201);
        }
        return response()->json([
            'succes' => false,
            'message' => 'author data failed to save'
        ], 409);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $author = Pengarang::findOrfail($id);

        // make json response
        return response()->json([
            'succes' => true,
            'message' => 'detail pengarang',
            'data' => $author
        ], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'nama' => 'required',
            'email' => 'required',
            'username' => 'required',
            'password' => 'required',
        ]);

        // response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }
        $author = Pengarang::findOrFail($id);
        if ($author) {
            if (auth()->user()->id == $author->user->id) {
                # code...
                $author->update([
                    'nama' => $request->nama,
                    'email' => $request->email,
                    'username' => $request->username,
                    'password' => $request->password,
                ]);
                return response()->json([
                    'succes' => true,
                    'message' => 'pengarang berhasil di updated',
                    'data' => $author
                ], 200);
            }
            return response()->json([
                'succes' => false,
                'message' => 'forbidden'
            ], 403);
        }
        return response()->json([
            'succes' => false,
            'message' => 'author not found'
        ], 404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $author = Pengarang::findOrFail($id);
        // deleted comment
        if ($author) {
            if (auth()->user()->id == $author->user->id) {
                # code...
                $author->delete();

                return response()->json([
                    'succes' => true,
                    'message' => 'pengarang berhasil di hapus'
                ], 200);
            }
            return response()->json([
                'succes' => false,
                'message' => 'forbidden'
            ], 403);
        }
        // response failed to delete
        return response()->json([
            'succes' => false,
            'message' => 'comment not found'
        ], 404);
    }
}
