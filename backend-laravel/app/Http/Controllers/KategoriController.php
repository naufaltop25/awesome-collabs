<?php

namespace App\Http\Controllers;

use App\Kategori;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class KategoriController extends Controller
{
    /**
     * index
     *
     * @return void
     */
    public function index()
    {
        $kategoris = Kategori::latest()->get();

        //make response JSON
        return response()->json([
            'success' => true,
            'message' => 'Daftar Kategori',
            'data'    => $kategoris  
        ], 200);

    }
    
     /**
     * show
     *
     * @param  mixed $id
     * @return void
     */
    public function show($id)
    {
        $kategori = Kategori::findOrfail($id);

        //make response JSON
        return response()->json([
            'success' => true,
            'message' => 'Detail Kategori',
            'data'    => $kategori 
        ], 200);

    }
    
    /**
     * store
     *
     * @param  mixed $request
     * @return void
     */
    public function store(Request $request)
    {
        //set validation
        $validator = Validator::make($request->all(), [
            'nama'   => 'required',
            'deskripsi' => 'required',
        ]);
        
        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        //save to database
        $kategori = Kategori::create([
            'nama'     => $request->nama,
            'deskripsi'   => $request->deskripsi
        ]);

        //success save to database
        if($kategori) {
            return response()->json([
                'success' => true,
                'message' => 'Kategori berhasil dibuat!',
                'data'    => $kategori  
            ], 201);

        } 

        //failed save to database
        return response()->json([
            'success' => false,
            'message' => 'Kategori Failed to Save',
        ], 409);

    }
    
    /**
     * update
     *
     * @param  mixed $request
     * @param  mixed $post
     * @return void
     */
    public function update(Request $request, Kategori $kategori)
    {
        //set validation
        $validator = Validator::make($request->all(), [
            'nama'   => 'required',
            'deskripsi' => 'required',
        ]);
        
        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $kategori = Kategori::findOrFail($kategori->id);

        if($kategori) {

            $kategori->update([
                'nama'   => $request->nama,
                'deskripsi' => $request->deskripsi,
            ]);

            return response()->json([
                'success' => true,
                'message' => 'Kategori berhasil diubah!',
                'data'    => $kategori  
            ], 200);

        }

        return response()->json([
            'success' => false,
            'message' => 'Kategori Not Found',
        ], 404);

    }
    
    /**
     * destroy
     *
     * @param  mixed $id
     * @return void
     */
    public function destroy($id)
    {
        $kategori = Kategori::findOrfail($id);

        if($kategori) {

            $kategori->delete();

            return response()->json([
                'success' => true,
                'message' => 'Kategori berhasil dihapus!',
            ], 200);

        }

        return response()->json([
            'success' => false,
            'message' => 'Kategori Not Found',
        ], 404);
    }
}
