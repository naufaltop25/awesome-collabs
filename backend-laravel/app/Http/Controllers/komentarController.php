<?php

namespace App\Http\Controllers;

use App\Komentar;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class komentarController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $komentar = Komentar::latest()->get();

        // make json response
        return response()->json([
            'succes' => true,
            'message' => 'list data comment',
            'data' => $komentar
        ], 200);
    }



    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'isi'   => 'required',
            'berita_id' => 'required',
        ]);

        // response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }
        // save to database
        $komen = Komentar::create([
            'isi' => $request->isi,
            'berita_id' => $request->berita_id,
            'user_id' => auth()->user()->id,
        ]);
        if ($komen) {
            return response()->json([
                'succes' => true,
                'message' => 'komentar berhasil di buat',
                'data' => $komen
            ], 201);
        }
        return response()->json([
            'succes' => false,
            'message' => 'comment data failed to save'
        ], 409);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $comment = Komentar::findOrfail($id);

        // make json response
        return response()->json([
            'succes' => true,
            'message' => 'detail komentar',
            'data' => $comment
        ], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'isi'   => 'required',
            'berita_id' => 'required',
        ]);

        // response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }
        $comment = Komentar::findOrFail($id);
        if ($comment) {
            if (auth()->user()->id == $comment->user->id) {
                # code...
                $comment->update([
                    'isi' => $request->isi,
                    'berita_id' => $request->berita_id
                ]);
                return response()->json([
                    'succes' => true,
                    'message' => 'komentar berhasil di updated',
                    'data' => $comment
                ], 200);
            }
            return response()->json([
                'succes' => false,
                'message' => 'forbidden'
            ], 403);
        }
        return response()->json([
            'succes' => false,
            'message' => 'comment not found'
        ], 404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $comment = Komentar::findOrFail($id);
        // deleted comment
        if ($comment) {
            if (auth()->user()->id == $comment->user->id) {
                # code...
                $comment->delete();

                return response()->json([
                    'succes' => true,
                    'message' => 'komentar berhasil di hapus'
                ], 200);
            }
            return response()->json([
                'succes' => false,
                'message' => 'forbidden'
            ], 403);
        }
        // response failed to delete
        return response()->json([
            'succes' => false,
            'message' => 'comment not found'
        ], 404);
    }
}
