<?php

namespace App\Http\Controllers;

use App\Berita;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;


class BeritaController extends Controller
{
    /**
     * index
     *
     * @return void
     */
    public function index()
    {
        $beritas = Berita::latest()->paginate(2);

        //make response JSON
        return response()->json([
            'success' => true,
            'message' => 'Daftar Berita',
            'data'    => $beritas
        ], 200);
    }

    /**
     * show
     *
     * @param  mixed $id
     * @return void
     */
    public function show($id)
    {
        $berita = Berita::findOrfail($id);

        //make response JSON
        return response()->json([
            'success' => true,
            'message' => 'Detail Berita',
            'data'    => $berita
        ], 200);
    }

    /**
     * store
     *
     * @param  mixed $request
     * @return void
     */
    public function store(Request $request)
    {
        //set validation
        $validator = Validator::make($request->all(), [
            'judul'   => 'required',
            'isi' => 'required',
            'kategori_id' => 'required',
        ]);

        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        //save to database
        $berita = Berita::create([
            'judul'     => $request->judul,
            'isi'   => $request->isi,
            'kategori_id'   => $request->kategori_id,
            'user_id' => auth()->user()->id,
        ]);

        //success save to database
        if ($berita) {
            return response()->json([
                'success' => true,
                'message' => 'Berita berhasil dibuat!',
                'data'    => $berita
            ], 201);
        }

        //failed save to database
        return response()->json([
            'success' => false,
            'message' => 'Berita Failed to Save',
        ], 409);
    }

    /**
     * update
     *
     * @param  mixed $request
     * @param  mixed $post
     * @return void
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'judul'   => 'required',
            'isi' => 'required',
        ]);

        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $berita = Berita::find($id);

        if (auth()->user()->id != $berita->user->id) {
            return response()->json([
                'succes' => false,
                'message' => 'forbidden'
            ], 403);
        }
        if ($berita) {
            $berita->update([
                'judul'   => $request->judul,
                'isi' => $request->isi,
            ]);

            return response()->json([
                'success' => true,
                'message' => 'Berita berhasil diubah!',
                'data'    => $berita
            ], 200);
        }

        return response()->json([
            'success' => false,
            'message' => 'Berita Not Found',
        ], 404);
    }

    /**
     * destroy
     *
     * @param  mixed $id
     * @return void
     */
    public function destroy($id)
    {
        $berita = Berita::findOrfail($id);

        if (auth()->user()->id != $berita->user->id) {
            return response()->json([
                'succes' => false,
                'message' => 'forbidden'
            ], 403);
        }

        if ($berita) {

            $berita->delete();

            return response()->json([
                'success' => true,
                'message' => 'Berita berhasil dihapus!',
            ], 200);
        }

        return response()->json([
            'success' => false,
            'message' => 'Berita Not Found',
        ], 404);
    }
}
