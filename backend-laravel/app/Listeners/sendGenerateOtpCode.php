<?php

namespace App\Listeners;

use App\Events\GenerateOtpCode;
use App\Mail\GenerateOtpCodeMail;
use Illuminate\Support\Facades\Mail;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class sendGenerateOtpCode implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  GenerateOtpCode  $event
     * @return void
     */
    public function handle(GenerateOtpCode $event)
    {
        Mail::to($event->otp->user->email)->send(new GenerateOtpCodeMail($event->otp));
    }
}
