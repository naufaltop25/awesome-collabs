<?php

namespace App\Listeners;

use App\Mail\OtpCodeMail;
use App\Events\RegisterOtpCode;
use Illuminate\Support\Facades\Mail;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class sendOtpCode implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  RegisterOtpCode  $event
     * @return void
     */
    public function handle(RegisterOtpCode $event)
    {
        Mail::to($event->otp->user->email)->send(new OtpCodeMail($event->otp));
    }
}
