<?php

namespace App;

use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

class Berita extends Model
{
    protected $fillable = [
        'judul', 'isi', 'kategori_id', 'user_id'
    ];
    protected $primaryKey = 'id';
    protected $keyType = 'string';
    public $incrementing = false;

    public function kategori()
    {
        return $this->belongsTo('App\Kategori');
    }
    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function komentar()
    {
        return $this->hasMany('App\Komentar');
    }

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            if (empty($model->{$model->getKeyName()})) {
                $model->{$model->getKeyName()} = Str::uuid();
            }
        });
    }

    public function getCreatedAttribute()
    {
        return Carbon::parse($this->attributes['created_at'])->translatedFormat('1, d F Y');
    }
}
