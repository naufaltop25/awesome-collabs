<?php

namespace App\Events;

use App\Otp_code;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class RegisterOtpCode
{
    use Dispatchable, InteractsWithSockets, SerializesModels;
    public $otp;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Otp_code $otp)
    {
        $this->otp = $otp;
    }
}
