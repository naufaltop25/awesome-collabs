<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware(['auth:api'])->group(function () {
    Route::apiResource('/berita', 'BeritaController')->except('index', 'show');
    Route::apiResource('/komentar', 'komentarController')->except('index', 'show');
    Route::apiResource('/kategori', 'KategoriController')->except('index', 'show');
});
Route::apiResource('/kategori', 'KategoriController')->only('index', 'show');
Route::apiResource('/berita', 'BeritaController')->only('index', 'show');
Route::apiResource('/komentar', 'komentarController')->only('index', 'show');

Route::prefix('auth')->namespace('Auth')->group(function () {
    Route::post('/register', 'RegisterController')->name('auth.register');
    Route::post('/regenerate-otp-code', 'RegenerateOtpController')->name('auth.regenerate');
    Route::post('/verification-otp', 'verificationOtpController')->name('auth.verification');
    Route::post('/update-password', 'updatePasswordController')->name('auth.update_password');
    Route::post('/login', 'LoginController')->name('auth.login');
});
