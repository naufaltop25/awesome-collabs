import Vue from 'vue'
import App from './App.vue'
import router from './router'
import vuetify from './plugins/vuetify'
import axios from './plugins/axios'
import moment from 'moment'

Vue.config.productionTip = false;
require('moment/locale/id');
Vue.use(require('vue-moment'), {moment});

new Vue({
  router,
  vuetify,
  axios,
  render: h => h(App)
}).$mount('#app')
